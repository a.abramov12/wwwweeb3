<!DOCTYPE html>
<html lang="ru">
<head >
	    <title>Web3</title>
	    <link rel="stylesheet" href="style.css">  
	    <meta charset="utf-8">
</head>
<body>
	<header>
	<div id="golova">
	<img src="LitMir.jpg" alt="LitMir" id="logo"/>
	<h1> Перятинская Евгения <br/> Задание №3 </h1></div>
	</header>
	<div id="main-aside-wrapper">
	<nav>
		
		<a href="#ssilk" id="m1">Гиперссылки</a>
		<a href="#tabl" id="m2">Таблицы</a>	
		<a href="#form" id="m3">Формы</a>
	</nav>
	
    <main>
	<div id="form" class="content">
		<h2 id="an" >Форма</h2>
	
	    <form action=" "
	      method="POST">
	
	      <label>
	        Имя:<br />
	        <input name="field-name-1"
	          value="Женя" />
	      </label><br />
	      
	     <label>
			email:<br />
			<input name="field-email"
			value="test@example.com"
			type="email" />
			</label><br />
	
	      <label>
		  	Дата Рождения:<br />
			<input name="field-date"
			value="2020-09-01"
			type="date" />
			</label><br />
	
	      
	        Пол:<br />
	      <label><input type="radio" checked="checked"
	        name="radio-group-1" value="М" />
	       М</label>
	      <label><input type="radio"
	        name="radio-group-1" value="Ж" />
	        Ж</label><br />
	        
	        
	      Количество конечностей:<br />
	      <label><input type="radio" checked="checked"
	        name="radio-group-2" value="1" />
	        1</label>
	      <label><input type="radio"
	        name="radio-group-2" value="2" />
	        2</label><br/>
	        <label><input type="radio"
	        name="radio-group-2" value="3" />
	        3</label>
	        <label><input type="radio"
	        name="radio-group-2" value="4" />
	        4</label><br/>
	        <label><input type="radio"
	        name="radio-group-2" value="Более 4" />
	        Больше</label><br />
	
	       <label>
	        Суперсила:
	        <br />
	        <select name="field-name-4[]"
	          multiple="multiple">
	          <option value="Значение1">бессмертие</option>
	          <option value="Значение2" selected="selected">прохождение сквозь стены</option>
	          <option value="Значение3" selected="selected">чтение мыслей</option>
	          <option value="Значение4" selected="selected">суперкулак</option>
	        </select>
	      </label><br />
	      
	        <label>
	        Биография:<br />
	        <textarea name="field-name-2"> Я жила в другом городе...
	        </textarea>
	      </label><br />
	      
	      Со всем выше написанным ознакомлен:<br />
	      <label><input type="checkbox" checked="checked"
	        name="check-1" />
	        ДА</label><br />
	
	      
	      <input type="submit" value="Отправить" />
	    </form>
	</div>
    </main>
	</div>
	<footer>
		<p>(с)Peryatinskaya Evgeniya 2020</p>
	</footer>
</body>
</html>
